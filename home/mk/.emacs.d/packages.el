(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(use-package paren
  :ensure t
  :init   (setq show-paren-delay 0)
  :config (show-paren-mode 1))

(use-package lsp-mode
  :ensure   t
  :commands lsp
  :hook     (sh-mode . lsp))

;;(use-package lsp-clang :ensure t)

(use-package ivy
  :ensure  t
  :config  (ivy-mode 1)
  :delight)

(use-package which-key
  :ensure t
  :init   (setq
            which-key-sort-order #'which-key-prefix-then-key-order
            which-key-separator " "
            which-key-prefix-prefix "+"
            which-key-sort-uppercase-first nil
            which-key-add-column-padding 1
            which-key-max-display-columns nil
            which-key-min-display-lines 5)
  :config (which-key-mode 1)
  :delight)

(use-package yasnippet
  :ensure t
  :config (yas-global-mode)
  :delight)

(use-package auto-yasnippet
  :ensure t
  :bind (("C-c a c" . aya-create) ("C-c a e" . aya-expand)))

(use-package yasnippet-snippets          :ensure t)
(use-package yasnippet-classic-snippets  :ensure t)

(use-package multi-term :ensure t)

(use-package company
  :ensure t
  :delight
  :init
  (setq
    company-dabbrev-downcase 0
    company-idle-delay 0
    company-frontends '(company-pseudo-tooltip-unless-just-one-frontend
                         company-preview-frontend
                         company-echo-metadata-frontend))
  :config
  (global-company-mode 1)
  (define-key company-active-map
    (kbd "TAB") 'company-complete-common-or-cycle)
  (define-key company-active-map
    (kbd "<tab>") 'company-complete-common-or-cycle)
  (define-key company-active-map
    (kbd "S-TAB") 'company-select-previous)
  (define-key company-active-map
    (kbd "<backtab>") 'company-select-previous))

(use-package company-auctex    :ensure t)
(use-package company-c-headers :ensure t)
(use-package company-lsp       :ensure t)
(use-package company-math      :ensure t)
(use-package company-shell     :ensure t)

(use-package dumb-jump :ensure t)
(use-package elisp-def :ensure t)

(use-package htmlize :ensure t)

(use-package org
  :ensure t
  :init
  ;; change postamble, to show last update date
  (defun my-org-html-postamble (plist)
    (format "Last update: %s" (format-time-string "%Y-%m-%d %H:%M")))
  (setq
    org-startup-with-inline-images t
    org-image-actual-width nil
    org-descriptive-links nil
    org-export-with-title nil
    org-html-postamble 'my-org-html-postamble
    org-html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"/res/org.css\"/>"
    org-publish-project-alist
    '(("blog-notes"
        :base-directory       "~/Documents/blogs/cresiopan.github.io/org/"
        :base-extension       "org"
        :publishing-directory "~/Documents/blogs/cresiopan.github.io/"
        :recursive            t
        :section-numbers      nil
        :with-author          nil
        :with-creator         nil
        :with-toc             nil
        :with-fixed-width     t
        :headline-levels      6
        :with-title           nil
        :publishing-function  org-html-publish-to-html)
       ("blog-notes"
        :base-directory       "~/Documents/blogs/test/org/"
        :base-extension       "org"
        :publishing-directory "~/Documents/blogs/test/html/"
        :recursive            t
        :section-numbers      nil
        :with-author          nil
        :with-creator         nil
        :with-toc             nil
        :with-fixed-width     t
        :headline-levels      6
        :with-title           nil
        :publishing-function  org-html-publish-to-html)))

  :config
  (org-babel-do-load-languages
    'org-babel-load-languages
    '(
       (gnuplot . t)
       (C . t)
       (org . t)
       (ditaa . t)
       (lisp . t)
       (python . t)
       (ruby . t)
       (dot . t)
       (java . t)
       (js . t)
       (shell . t)
       (latex . t)
       (sql . t)
       (sqlite . t)
       (emacs-lisp . t)))
  (setf org-html-mathjax-options
    '((path "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS_HTML")
       (linebreaks "true")
       (tagside "right")
       (align "center")
       (scale "120")
       (font "TeX")
       (indent "0"))
    org-html-mathjax-template
    "<script src=\'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML\' async></script>"))


(use-package multiple-cursors
  :ensure t
  :bind(("C-S-c C-S-c" . 'mc/edit-lines)
         ("C-<" . 'mc/mark-next-like-this)
         ("C->" . 'mc/mark-previous-like-this)
         ("C-c C-<" . 'mc/mark-all-like-this)))

(use-package dired
  :init (setq
          dired-listing-switches "-laGh --group-directories-first"
          find-name-arg "-iname"))

(use-package async
  :ensure t
  :init (dired-async-mode 1))

(use-package magit :ensure t)

(use-package expand-region
  :ensure t
  :bind (("C-+" . er/expand-region) ("C-*" . er/contract-region)))

(use-package browse-kill-ring
  :ensure t
  :bind ("C-c C-k" . 'browse-kill-ring))

(use-package drag-stuff
  :ensure t
  :delight
  :config (drag-stuff-global-mode 1)
  (drag-stuff-define-keys))

(use-package editorconfig
  :ensure t
  :delight
  :config (editorconfig-mode 1))

(use-package hideshow
  :ensure t
  :bind (("C-c x c" . hs-show-all)
          ("C-c x x" . hs-hide-all)
          ("C-c c" . toggle-fold)))

(use-package whitespace
  :config (setq whitespace-style '(face empty tabs lines-tail trailing))
  (global-whitespace-mode t))

(use-package dracula-theme :ensure t)
(use-package doom-themes :ensure t)
