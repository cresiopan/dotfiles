(defalias 'yes-or-no-p 'y-or-n-p)

(add-to-list 'load-path "~/.emacs.d/lisp/")

(put 'find-alternate-file 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'capitalize-region 'disabled nil)

;;(eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))

(setq mouse-autoselect-window t
  focus-follows-mouse t
  inhibit-splash-screen t
  inhibit-startup-message t
  initial-scratch-message ""
  search-highlight t
  query-replace-highlight t
  backup-directory-alist '(("." . "~/.emacs.d/backups"))
  tab-width 4
  tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60
                   64 68 72 76 80 84 88 92 96 100 104 108 112
                   116 120))

(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(blink-cursor-mode -1)
(electric-pair-mode)
(global-hl-line-mode 1)
(delete-selection-mode 1)

;; Mode line settings
;;(setq-default mode-line-format
;;  (list
;;    " "
;;    ;; the buffer name; the file name as a tool tip
;;    (propertize "%b"
;;      'face
;;      '(help-echo (buffer-file-name)))
;;    ;; line and column
;;    " (" (propertize "%02l" 'face 'font-lock-keyword-face)
;;    ","  (propertize "%02c" 'face 'font-lock-keyword-face) ") "
;;    ;; spaces to align right
;;    '(:eval
;;       (propertize " "
;;         'display
;;         `((space :align-to
;;             (- (+ right right-fringe right-margin)
;;               ,(+ 3 (string-width mode-name)
;;                  (string-width (format-time-string " %a %d %b %H:%M:%S "))
;;                  7
;;                  ))))))
;;    ;; the current major mode
;;    (propertize " %m " 'face 'font-lock-string-face)
;;    ;; battery status
;;    "[" (propertize
;;          (format "%03s" (cresiopan-battery-check (battery-linux-sysfs)))
;;          'face 'font-lock-string-face)
;;    "%%]"
;;    ;; day and time
;;    (propertize (format-time-string " %a %d %b %H:%M:%S ")
;;      'face 'font-lock-builtin-face)
;;    ))

(setq-default indent-tabs-mode nil
  fill-column 80)

(set-face-attribute 'default nil
  :font "Source Code Pro"
  :width 'normal
  :weight 'normal
  :height 130)

;;(add-to-list 'default-frame-alist '(font   . "Source Code Pro"))
(add-to-list 'default-frame-alist '(width  . 80))
(add-to-list 'default-frame-alist '(height . 24))

(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(if (file-exists-p (expand-file-name "~/.emacs.d/functions.el"))
  (load-file (expand-file-name "~/.emacs.d/functions.el")))

(if (file-exists-p (expand-file-name "~/.emacs.d/bindings.el"))
  (load-file (expand-file-name "~/.emacs.d/bindings.el")))

(if (file-exists-p (expand-file-name "~/.emacs.d/packages.el"))
  (load-file (expand-file-name "~/.emacs.d/packages.el")))

;;(with-current-buffer (get-buffer-create "output")(insert "some text"))

(load-file (expand-file-name "~/.emacs.d/term.el"))

(defun zmk-activate-exwm()
  (progn
    ((require 'exwm)
      (require 'exwm-config)
      (exwm-config-default)

      ;;(print exwm-workspace--current)

      (setq exwm-input-global-keys
        `(
           ([?\s-r] . exwm-reset)           ;; 's-r': Reset (to line-mode).
           ([?\s-w] . exwm-workspace-switch);; 's-w': Switch workspace.
           ([?\s-&] . (lambda (command)     ;; 's-&': Launch application.
                        (interactive (list (read-shell-command "$ ")))
                        (start-process-shell-command command nil command)))
           ([s-return] . +term/there)
           ([s-f1] . (lambda()(interactive)(start-process-shell-command "Firefox" nil "firefox")))
           ([s-tab] . next-buffer)
           ;;([<XF86AudioRaiseVolume] . (lambda()(interactive)(start-process-shell-command  "volume +"  "*Messages*"  "volume +")))
           ;;([<XF86AudioLowerVolume] . (lambda()(interactive)(start-process-shell-command  "volume -"  "*Messages*"  "volume -")))
           ;;([<XF86MonBrightnessUp] . (lambda()(interactive)(start-process-shell-command  "brightness +"  "*Messages*"  "brightness +")))
           ;;([<XF86MonBrightnessDown] . (lambda()(interactive)(start-process-shell-command  "brightness -"  "*Messages*"  "brightness -")))
           ;; 's-N': Switch to certain workspace.
           ,@(mapcar (lambda (i)
                       `(,(kbd (format "s-%d" i)) .
                          (lambda ()
                            (interactive)
                            (exwm-workspace-switch-create ,i))))
               (number-sequence 1 4)))
        exwm-workspace-number 4)

      ;;     ("C-c C-f" . exwm-layout-set-fullscreen)
      ;;     ("C-c C-h" . exwm-floating-hide)
      ;;     ("C-c C-k" . exwm-input-release-keyboard)
      ;;     ("C-c C-m" . exwm-workspace-move-window)
      ;;     ("C-c C-q" . exwm-input-send-next-key)
      ;;     ("C-c C-t C-f" . exwm-floating-toggle-floating)
      ;;     ("C-c C-t C-m" . exwm-layout-toggle-mode-line)
      )))
