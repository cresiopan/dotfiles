(eval-after-load 'org
  (progn
    (require 'org)
    (define-key org-mode-map (kbd "C-c C-f") nil)
    (define-key org-mode-map (kbd "C-c C-s") nil)
    (define-key org-mode-map (kbd "C-c C-d") nil)
    (define-key org-mode-map (kbd "C-j") nil)
    (define-key org-mode-map (kbd "C-k") nil)
    (define-key org-mode-map (kbd "C-d") nil)
    (define-key org-mode-map (kbd "C-l") nil)
    (define-key org-mode-map (kbd "<C-tab>") nil)))

(global-unset-key (kbd "C-z")) ;; suspend emacs
(global-unset-key (kbd "M-z")) ;; zap to char
(global-unset-key (kbd "C-t")) ;; transpose-chars

(global-set-key (kbd "s-q") 'delete-window)
(global-set-key (kbd "s-c") 'kill-current-buffer)
;;(global-set-key (kbd "<s-return>") 'delete-other-windows)


;; C-x o
(global-set-key (kbd "<C-tab>") (lambda () (interactive) (other-window 1)))

(global-set-key (kbd "s-<tab>") (lambda () (interactive) (next-buffer)))
(global-set-key (kbd "<s-iso-lefttab>") (lambda () (interactive) (previous-buffer)))

;; reload configuration
(global-set-key (kbd "C-c r")
  (lambda ()
    (interactive)
    (load-file (expand-file-name "~/.emacs.d/mk.el"))))
;;    (org-babel-load-file "~/.emacs.d/mk.org")))

;;(global-set-key (kbd "C-j") 'backward-char)
;;(global-set-key (kbd "C--") 'forward-char)
;;(global-set-key (kbd "C-;") 'forward-char)
;;(global-set-key (kbd "C-ñ") 'forward-char)

;;(global-set-key (kbd "C-k") 'next-line)
;;(global-set-key (kbd "C-l") 'previous-line)
;;(global-set-key (kbd "M-j") 'backward-word)
;;(global-set-key (kbd "M--") 'forward-word)
;;(global-set-key (kbd "M-;") 'forward-word)
;;(global-set-key (kbd "M-ñ") 'forward-word)

;;(global-set-key (kbd "M-k") 'forward-paragraph)
;;(global-set-key (kbd "M-l") 'backward-paragraph)

(global-set-key (kbd "M-n") 'forward-paragraph)
(global-set-key (kbd "M-p") 'backward-paragraph)

;;(global-set-key (kbd "C-f") 'keyboard-quit)
;;(global-set-key (kbd "C-p") 'recenter-top-bottom)

;;(global-set-key (kbd "C-f")
;;  (lambda ()
;;    (interactive)
;;    (find-name-dired "/home/mk" (read-from-minibuffer "find-name: "))))

(global-set-key (kbd "C-r") 'replace-string)
(global-set-key (kbd "C-M-r") 'replace-regexp)

(global-set-key (kbd "s-M-k") 'delete-window)
(global-set-key (kbd "s-C-k") 'kill-buffer)

(global-set-key "\C-c\C-d" "\C-a\C- \C-e\M-w\C-o\C-y  ")
(global-set-key (kbd "<f12>") 'cresiopan-fix-text)
(global-set-key "%" 'cresiopan-match-paren)

(global-set-key (kbd "C-M-<backspace>") 'cresiopan-kill-whole-line)
(global-set-key (kbd "C-<backspace>") 'cresiopan-backward-kill-char-or-word)
(global-set-key (kbd "C-o") 'cresiopan-open-line-below-and-switch)
(global-set-key (kbd "C-M-o") 'cresiopan-open-line-up-and-switch)

(global-set-key (kbd "C-c h") 'split-window-below)
(global-set-key (kbd "C-c v") 'split-window-right)
(global-set-key (kbd "C-c g") 'goto-line)
(global-set-key (kbd "C-c b") 'bookmark-jump)
(global-set-key (kbd "C-c n") 'display-line-numbers-mode)
;;(global-set-key (kbd "C-c k") 'xah-previous-user-buffer)
;;(global-set-key (kbd "C-c l") 'xah-next-user-buffer)
(global-set-key (kbd "C-c C-s") 'cresiopan-open-file-with-ssh)
(global-set-key (kbd "C-c C-f") 'cresiopan-open-file-with-sudo)

(global-set-key (kbd "s-s") 'cresiopan-make-split)
(global-set-key (kbd "s-m") 'cresiopan-open-new-empty-buffer-in-new-window)
(global-set-key (kbd "s-x") 'cresiopan-open-terminal-in-new-window)
(global-set-key (kbd "s-n") 'xah-new-empty-buffer)
(global-set-key (kbd "s-z") 'cresiopan-open-terminal-in-current-window)
