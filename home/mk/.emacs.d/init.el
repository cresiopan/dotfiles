(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("gnu"   . "https://elpa.gnu.org/packages/")))

(package-initialize)

(if (file-exists-p (expand-file-name "~/.emacs.d/mk.el"))
  (load-file (expand-file-name "~/.emacs.d/mk.el")))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("7ab17951238d1ac0dddc85120d09842257f74048843aff1709767d02f5efa2bf" "5ee12d8250b0952deefc88814cf0672327d7ee70b16344372db9460e9a0e3ffc" default)))
 '(package-selected-packages
   (quote
    (doom-themes magit exwm-config emacs-snippets elisp-def dumb-jump company-shell company-log-mode multi-term dracula-theme company-auctex company-c-headers company-lsp company-math mmm-mode pdf-tools pdf-tool fzf y centered-window treemacs nov editorconfig robe drag-stuff browse-kill-ring expand-region async multiple-cursors htmlize company yasnippet-snippets yasnippet-classic-snippets auto-yasnippet yasnippet which-key ivy use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-scrollbar-bg ((t (:background "#feffff"))))
 '(company-scrollbar-fg ((t (:background "#feffff"))))
 '(company-tooltip ((t (:inherit default :background "#000000"))))
 '(company-tooltip-common ((t (:inherit font-lock-constant-face))))
 '(company-tooltip-selection ((t (:inherit font-lock-function-name-face)))))
