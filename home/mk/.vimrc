"call plug#begin()
" -------------------------------------
"Plug 'andreypopp/vim-colors-plain'
" -------------------------------------
"call plug#end()

" =====================================
" Initial settings
" =====================================

" Relax file compatibility restriction with original vi
" (not necessary to set with neovim, but useful for vim)
set nocompatible

" Disable beep / flash
set vb t_vb=

" Set tabs and indents (for go)
set ts=4
set shiftwidth=4
set ai sw=4
" replace tab with spaces
set expandtab
" allow cursor to move to beginning of tab
" will interfere with soft line wrapping (set nolist)
set list lcs=tab:\ \ 

" highlight matches when searching
set hlsearch

" Line numbering
set number

" Disable line wrapping
set wrap

" enable line and column display
set ruler

"disable showmode since using vim-airline; otherwise use 'set showmode'
set noshowmode

" file type recognition
filetype on
filetype plugin on
filetype indent on

" syntax highlighting
syntax on

" scroll a bit horizontally when at the end of the line
set sidescroll=6

" mouse support
set mouse=a

" Make it easier to work with buffers
set hidden
set confirm
set autowriteall
set wildmenu wildmode=full

" open new split panes to right and below (as you probably expect)
set splitright
set splitbelow

" =====================================
" Theme color scheme settings
" =====================================

function! Light()
    echom "set bg=light"
    set bg=light
    colorscheme default
    set list
endfunction

function! Dark()
    echom "set bg=dark"
    set bg=dark
    colorscheme default
    set nolist
endfunction

function! ToggleLightDark()
  if &bg ==# "light"
    call Dark()
  else
    call Light()
  endif
endfunction

" Disable scrollbar in gui
" hide right scrollbar
set guioptions-=r
"
set guifont=Menlo\ Regular:h16

" =====================================
" key map
" Understand mapping modes:
" =====================================

" change the leader key
let mapleader=" "

" Shortcut to edit THIS configuration file: (e)dit (c)onfiguration
nnoremap <silent> <leader>ec :e $MYVIMRC<CR>

" Shortcut to source (reload) THIS configuration file after editing it: (s)ource (c)onfiguraiton
nnoremap <silent> <leader>sc :source $MYVIMRC<CR>

" toggle buffer (switch between current and last buffer)
nnoremap <silent> <leader>bb <C-^>

" go to next buffer
nnoremap <silent> <leader>bn :bn<CR>
nnoremap <C-l> :bn<CR>

" go to previous buffer
nnoremap <silent> <leader>bp :bp<CR>
" https://github.com/neovim/neovim/issues/2048
nnoremap <C-h> :bp<CR>

" close buffer
nnoremap <silent> <leader>bd :bd<CR>

" kill buffer
nnoremap <silent> <leader>bk :bd!<CR>

" list buffers
nnoremap <silent> <leader>bl :ls<CR>
" list and select buffer
nnoremap <silent> <leader>bg :ls<CR>:buffer<Space>

" horizontal split with new buffer
nnoremap <silent> <leader>bh :new<CR>

" vertical split with new buffer
nnoremap <silent> <leader>bv :vnew<CR>

" redraw screan and clear search highlighted items
nnoremap <silent> <C-L> :nohlsearch<CR><C-L>

" improved keyboard navigation
"nnoremap <leader>j <C-w><left>
"nnoremap <leader>k <C-w><down>
"nnoremap <leader>l <C-w><up>
"nnoremap <leader>ñ <C-w><right>
"nnoremap <leader>; <C-w><right>

" improved keyboard support for navigation (especially terminal)
tnoremap <Esc> <C-\><C-n>
"tnoremap <A-j> <C-\><C-n><C-w><left>
"tnoremap <A-k> <C-\><C-n><C-w><down>
"tnoremap <A-l> <C-\><C-n><C-w><up>
"tnoremap <A-ñ> <C-\><C-n><C-w><right>
"nnoremap <A-j> <C-w><left>
"nnoremap <A-k> <C-w><down>
"nnoremap <A-l> <C-w><up>
"nnoremap <A-ñ> <C-w><right>

" toggle colors to optimize based on light or dark background
nnoremap <leader>m :call ToggleLightDark()<CR>

" =====================================
" Custom styling
" =====================================

" http://stackoverflow.com/questions/9001337/vim-split-bar-styling
set fillchars+=vert:\ 

" http://vim.wikia.com/wiki/Highlight_current_line
" http://stackoverflow.com/questions/8750792/vim-highlight-the-whole-current-line
" http://vimdoc.sourceforge.net/htmldoc/autocmd.html#autocmd-events
autocmd BufEnter * setlocal cursorline
autocmd WinEnter * setlocal cursorline
autocmd BufLeave * setlocal nocursorline
autocmd WinLeave * setlocal nocursorline

" tagbar autopen
"autocmd VimEnter * nested :call tagbar#autoopen(1)
"autocmd FileType * nested :call tagbar#autoopen(0)
"autocmd BufEnter * nested :call tagbar#autoopen(0)

" =====================================
" Custom things
" =====================================
" movement
nnoremap j <left>
nnoremap k <down>
nnoremap l <up>
nnoremap ñ <right>
nnoremap ; <right>
nnoremap - <right>
vnoremap j <left>
vnoremap k <down>
vnoremap l <up>
vnoremap ñ <right>
vnoremap ; <right>
vnoremap - <right>

" search
"nnoremap - /

" save
nnoremap <leader>w <esc>:w<cr>
nnoremap <leader>W <esc>:w<cr>

" quit
nnoremap q <esc>:q<cr>
nnoremap Q <esc>:q<cr>

" copy
set clipboard+=unnamedplus
vnoremap <C-A-c> "+y
vnoremap <leader>c "+y

" paste
"inoremap <leader>v <esc>"+pi

set ignorecase
set smartcase
set foldmethod=marker

command Cl %s/[\t\s]\+$//g
command Clean %s/[\t\s]\+$//g
command W :w
command Q :q
command WQ :wq
command Wq :wq

let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 0

" =====================================
" Init
" =====================================
silent call Dark()
autocmd VimEnter * wincmd p

set encoding=utf-8 
    set fileencodings=utf-8
