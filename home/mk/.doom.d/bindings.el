;;; package --- Summary
;;; Commentary:

;;; Code:
;; taken from ~/.emacs.d/modules/config/default/+evil-bindings.el
(map!
 ;; Global evil keybinds
 :m  "]a"    #'evil-forward-arg
 :m  "[a"    #'evil-backward-arg
 :m  "]o"    #'outline-next-visible-heading
 :m  "[o"    #'outline-previous-visible-heading
 :n  "]b"    #'next-buffer
 :n  "[b"    #'previous-buffer
 :n  "zx"    #'kill-current-buffer
 :n  "ZX"    #'bury-buffer
 :n  "gp"    #'+evil/reselect-paste
 :n  "g="    #'evil-numbers/inc-at-pt
 :n  "g-"    #'evil-numbers/dec-at-pt
 :v  "g="    #'evil-numbers/inc-at-pt-incremental
 :v  "g-"    #'evil-numbers/dec-at-pt-incremental
 :v  "g+"    #'evil-numbers/inc-at-pt
 :nv "z="    #'flyspell-correct-word-generic
 :nv "g@"    #'+evil:apply-macro
 :nv "gc"    #'evil-commentary
 :nv "gx"    #'evil-exchange
 :v  "gp"    #'+evil/paste-preserve-register
 :v  "@"     #'+evil:apply-macro
 ;; repeat in visual mode (FIXME buggy)
 :v  "."     #'+evil:apply-macro
 ;; don't leave visual mode after shifting
 :v  "<"     #'+evil/visual-dedent  ; vnoremap < <gv
 :v  ">"     #'+evil/visual-indent  ; vnoremap > >gv

 ;; window management (prefix "C-w")
 (:map evil-window-map
   ;; Navigation
   "C-j"     #'evil-window-left
   "C-k"     #'evil-window-down
   "C-l"     #'evil-window-up
   "C--"     #'evil-window-right
   "C-w"     #'other-window
   ;; Swapping windows
   "J"       #'+evil/window-move-left
   "K"       #'+evil/window-move-down
   "L"       #'+evil/window-move-up
   "_"       #'+evil/window-move-right
   "C-S-w"   #'ace-swap-window
   ;; Window undo/redo
   (:prefix "m"
     "m"       #'doom/window-maximize-buffer
     "v"       #'doom/window-maximize-vertically
     "s"       #'doom/window-maximize-horizontally)
   "u"       #'winner-undo
   "C-r"     #'winner-redo
   "o"       #'doom/window-enlargen
   ;; Delete window
   "c"       #'+workspace/close-window-or-workspace
   "C-C"     #'ace-delete-window)

 ;; Plugins
 ;; evil-easymotion
 :m  "gs"    #'+evil/easymotion  ; lazy-load `evil-easymotion'
 (:after evil-easymotion
   :map evilem-map
   "a" (evilem-create #'evil-forward-arg)
   "A" (evilem-create #'evil-backward-arg)
   "s" (evilem-create #'evil-snipe-repeat
                      :name 'evil-easymotion-snipe-forward
                      :pre-hook (save-excursion (call-interactively #'evil-snipe-s))
                      :bind ((evil-snipe-scope 'buffer)
                             (evil-snipe-enable-highlight)
                             (evil-snipe-enable-incremental-highlight)))
   "S" (evilem-create #'evil-snipe-repeat
                      :name 'evil-easymotion-snipe-backward
                      :pre-hook (save-excursion (call-interactively #'evil-snipe-S))
                      :bind ((evil-snipe-scope 'buffer)
                             (evil-snipe-enable-highlight)
                             (evil-snipe-enable-incremental-highlight)))
   "SPC" #'avy-goto-char-timer
   "/" (evilem-create #'evil-ex-search-next
                      :pre-hook (save-excursion (call-interactively #'evil-ex-search-forward))
                      :bind ((evil-search-wrap)))
   "?" (evilem-create #'evil-ex-search-previous
                      :pre-hook (save-excursion (call-interactively #'evil-ex-search-backward))
                      :bind ((evil-search-wrap))))

 ;; text object plugins
 :textobj "x" #'evil-inner-xml-attr               #'evil-outer-xml-attr
 :textobj "a" #'evil-inner-arg                    #'evil-outer-arg
 :textobj "B" #'evil-textobj-anyblock-inner-block #'evil-textobj-anyblock-a-block
 :textobj "i" #'evil-indent-plus-i-indent         #'evil-indent-plus-a-indent
 :textobj "k" #'evil-indent-plus-i-indent-up      #'evil-indent-plus-a-indent-up
 :textobj "j" #'evil-indent-plus-i-indent-up-down #'evil-indent-plus-a-indent-up-down

 ;; evil-snipe
 (:after evil-snipe
   :map evil-snipe-parent-transient-map
   "C-;" (λ! (require 'evil-easymotion)
             (call-interactively
              (evilem-create #'evil-snipe-repeat
                             :bind ((evil-snipe-scope 'whole-buffer)
                                    (evil-snipe-enable-highlight)
                                    (evil-snipe-enable-incremental-highlight))))))

 ;; evil-surround
 :v "S" #'evil-surround-region
 :o "s" #'evil-surround-edit
 :o "S" #'evil-Surround-edit)

;;
;;; Module keybinds

;;; :completion
(map! (:when (featurep! :completion company)
        :i "C-@"      nil
        :i "C-SPC"    #'+company/complete
        (:prefix "C-x"
          :i "C-l"    #'+company/whole-lines
          :i "C-k"    nil
          :i "C-f"    #'company-files
          :i "C-]"    nil
          :i "s"      nil
          :i "C-s"    #'company-yasnippet
          :i "C-o"    nil
          :i "C-n"    nil
          :i "C-p"    nil)
        (:after company
          (:map company-active-map
            "C-w"     nil  ; don't interfere with `evil-delete-backward-word'
            "C-n"     #'company-select-next
            "C-k"     #'company-select-next
            "C-p"     #'company-select-previous
            "C-l"     #'company-select-previous
            "C-h"     #'company-show-doc-buffer
            "C-j"     #'company-previous-page
            "C--"     #'company-next-page
            "C-s"     #'company-filter-candidates
            "C-S-s"   #'counsel-company
            "C-SPC"   #'company-complete-common
            "TAB"     #'company-complete-common-or-cycle
            [tab]     #'company-complete-common-or-cycle
            [backtab] #'company-select-previous)

          (:map company-search-map  ; applies to `company-filter-map' too
            "C-n"     #'company-select-next-or-abort
            "C-p"     #'company-select-previous-or-abort
            "C-j"     #'company-select-next-or-abort
            "C-k"     #'company-select-previous-or-abort
            "C-s"     (λ! (company-search-abort) (company-filter-candidates))
            "ESC"     #'company-search-abort)
          ;; TAB auto-completion in term buffers
          (:map comint-mode-map
            "TAB" #'company-complete
            [tab] #'company-complete)))

      (:when (featurep! :completion ivy)
        (:map (help-mode-map helpful-mode-map)
          :n "Q" #'ivy-resume)
        (:after ivy
          :map ivy-minibuffer-map
          "C-SPC" #'ivy-call-and-recenter  ; preview file
          "C-l"   #'ivy-alt-done
          "C-v"   #'yank)
        (:after counsel
          :map counsel-ag-map
          "C-SPC"    #'ivy-call-and-recenter ; preview
          "C-l"      #'ivy-done
          [C-return] (+ivy-do-action! #'+ivy-git-grep-other-window-action)))
      )

;;; :ui
(map! (:when (featurep! :ui hl-todo)
        :m "]t" #'hl-todo-next
        :m "[t" #'hl-todo-previous)

      (:when (featurep! :ui vc-gutter)
        :m "]d"    #'git-gutter:next-hunk
        :m "[d"    #'git-gutter:previous-hunk)

      (:when (featurep! :ui workspaces)
        :n "gt"    #'+workspace/switch-right
        :n "gT"    #'+workspace/switch-left
        :n "]w"    #'+workspace/switch-right
        :n "[w"    #'+workspace/switch-left
        :g "M-1"   #'+workspace/switch-to-0
        :g "M-2"   #'+workspace/switch-to-1
        :g "M-3"   #'+workspace/switch-to-2
        :g "M-4"   #'+workspace/switch-to-3
        :g "M-5"   #'+workspace/switch-to-4
        :g "M-6"   #'+workspace/switch-to-5
        :g "M-7"   #'+workspace/switch-to-6
        :g "M-8"   #'+workspace/switch-to-7
        :g "M-9"   #'+workspace/switch-to-8
        :g "M-0"   #'+workspace/switch-to-final
        :g "M-t"   #'+workspace/new
        :g "M-T"   #'+workspace/display))

;; :editor
(map! (:when (featurep! :editor fold)
        :nv "C-SPC" #'+fold/toggle)

      (:when (featurep! :editor format)
        :n "gQ"    #'+format:region)

      (:when (featurep! :editor multiple-cursors)
        ;; evil-mc
        (:prefix "gz"
          :nv "d" #'evil-mc-make-and-goto-next-match
          :nv "D" #'evil-mc-make-and-goto-prev-match
          :nv "j" #'evil-mc-make-cursor-move-next-line
          :nv "k" #'evil-mc-make-cursor-move-prev-line
          :nv "m" #'evil-mc-make-all-cursors
          :nv "n" #'evil-mc-make-and-goto-next-cursor
          :nv "N" #'evil-mc-make-and-goto-last-cursor
          :nv "p" #'evil-mc-make-and-goto-prev-cursor
          :nv "P" #'evil-mc-make-and-goto-first-cursor
          :nv "q" #'evil-mc-undo-all-cursors
          :nv "t" #'+multiple-cursors/evil-mc-toggle-cursors
          :nv "u" #'evil-mc-undo-last-added-cursor
          :nv "z" #'+multiple-cursors/evil-mc-make-cursor-here)
        (:after evil-mc
          :map evil-mc-key-map
          :nv "C-n" #'evil-mc-make-and-goto-next-cursor
          :nv "C-N" #'evil-mc-make-and-goto-last-cursor
          :nv "C-p" #'evil-mc-make-and-goto-prev-cursor
          :nv "C-P" #'evil-mc-make-and-goto-first-cursor)
        ;; evil-multiedit
        :v  "R"     #'evil-multiedit-match-all
        :n  "M-d"   #'evil-multiedit-match-symbol-and-next
        :n  "M-D"   #'evil-multiedit-match-symbol-and-prev
        :v  "M-d"   #'evil-multiedit-match-and-next
        :v  "M-D"   #'evil-multiedit-match-and-prev
        :nv "C-M-d" #'evil-multiedit-restore
        (:after evil-multiedit
          (:map evil-multiedit-state-map
            "M-d"    #'evil-multiedit-match-and-next
            "M-D"    #'evil-multiedit-match-and-prev
            "RET"    #'evil-multiedit-toggle-or-restrict-region
            [return] #'evil-multiedit-toggle-or-restrict-region)
          (:map (evil-multiedit-state-map evil-multiedit-insert-state-map)
            "C-n" #'evil-multiedit-next
            "C-p" #'evil-multiedit-prev)))

      (:when (featurep! :editor snippets)
        ;; yasnippet
        (:after yasnippet
          (:map yas-keymap
            "C-e"         #'+snippets/goto-end-of-field
            "C-a"         #'+snippets/goto-start-of-field
            [M-right]     #'+snippets/goto-end-of-field
            [M-left]      #'+snippets/goto-start-of-field
            [M-backspace] #'+snippets/delete-to-start-of-field
            [backspace]   #'+snippets/delete-backward-char
            [delete]      #'+snippets/delete-forward-char-or-field))))

;;
;;; <leader>

(map! :leader
      :desc "Eval expression"       ";"    #'eval-expression
      :desc "M-x"                   ":"    #'execute-extended-command
      :desc "Pop up scratch buffer" "x"    #'doom/open-scratch-buffer
      :desc "Org Capture"           "X"    #'org-capture

      ;; C-u is used by evil
      :desc "Universal argument"    "u"    #'universal-argument
      :desc "window"                "w"    evil-window-map
      :desc "help"                  "h"    help-map

      :desc "Toggle last popup"     "~"    #'+popup/toggle
      :desc "Find file"             "."    #'find-file

      :desc "Switch buffer"         ","    #'switch-to-buffer
      (:when (featurep! :ui workspaces)
        :desc "Switch workspace buffer" "," #'persp-switch-to-buffer
        :desc "Switch buffer"           "<" #'switch-to-buffer)

      :desc "Resume last search"    "'"
      (cond ((featurep! :completion ivy)   #'ivy-resume))

      :desc "Search for symbol in project" "*" #'+default/search-project-for-symbol-at-point

      :desc "Find file in project"  "SPC"  #'projectile-find-file
      :desc "Blink cursor line"     "DEL"  #'+nav-flash/blink-cursor
      :desc "Jump to bookmark"      "RET"  #'bookmark-jump

      ;;; <leader> / --- search
      (:prefix-map ("/" . "search")
        :desc "Search buffer"                 "b" #'swiper
        :desc "Search current directory"      "d" #'+default/search-from-cwd
        :desc "Jump to symbol"                "i" #'imenu
        :desc "Jump to link"                  "l" #'ace-link
        :desc "Look up online"                "o" #'+lookup/online-select
        :desc "Look up in local docsets"      "k" #'+lookup/in-docsets
        :desc "Look up in all docsets"        "K" #'+lookup/in-all-docsets
        :desc "Search project"                "p" #'+default/search-project)

      ;;; <leader> TAB --- workspace
      (:when (featurep! :ui workspaces)
        (:prefix-map ("TAB" . "workspace")
          :desc "Display tab bar"           "TAB" #'+workspace/display
          :desc "Switch workspace"          "."   #'+workspace/switch-to
          :desc "New workspace"             "n"   #'+workspace/new
          :desc "Delete this workspace"     "k"   #'+workspace/delete
          :desc "Next workspace"            "f"   #'+workspace/switch-right
          :desc "Previous workspace"        "b"   #'+workspace/switch-left
          :desc "Switch to 1st"             "1"   #'+workspace/switch-to-0
          :desc "Switch to 2nd"             "2"   #'+workspace/switch-to-1
          :desc "Switch to 3rd"             "3"   #'+workspace/switch-to-2
          :desc "Switch to 4th"             "4"   #'+workspace/switch-to-3
          :desc "Switch to 5th"             "5"   #'+workspace/switch-to-4
          :desc "Switch to 6th"             "6"   #'+workspace/switch-to-5
          :desc "Switch to 7th"             "7"   #'+workspace/switch-to-6
          :desc "Switch to 8th"             "8"   #'+workspace/switch-to-7
          :desc "Switch to 9th"             "9"   #'+workspace/switch-to-8
          :desc "Switch to final workspace" "0"   #'+workspace/switch-to-final
          :n                                "`"   nil
          :n                                "x"   nil
          :n                                "l"   nil
          :n                                "s"   nil
          :n                                "r"   nil
          :n                                "R"   nil
          ))

      ;;; <leader> b --- buffer
      (:prefix-map ("b" . "buffer")
        (:when (featurep! :ui workspaces)
          :desc "Switch workspace buffer"   "b" #'persp-switch-to-buffer
          :desc "Switch buffer"             "B" #'switch-to-buffer)
        (:unless (featurep! :ui workspaces)
          :desc "Switch buffer"             "b" #'switch-to-buffer)
        :desc "Kill buffer"                 "k"   #'kill-current-buffer
        :desc "Next buffer"                 "f"   #'next-buffer
        :desc "New empty buffer"            "n"   #'evil-buffer-new
        :desc "Previous buffer"             "b"   #'previous-buffer
        :desc "Save buffer"                 "s"   #'save-buffer
        :desc "Sudo edit this file"         "S"   #'doom/sudo-this-file
        :n                                  "-"   nil
        :n                                  "["   nil
        :n                                  "]"   nil
        :n                                  "o"   nil
        :n                                  "x"   nil
        :n                                  "X"   nil
        :n                                  "z"   nil
        )

      ;;; <leader> c --- code
      (:prefix-map ("c" . "code")
        :desc "Jump to definition"          "d"   #'+lookup/definition
        :desc "Jump to references"          "D"   #'+lookup/references
        :desc "Evaluate buffer/region"      "e"   #'+eval/buffer-or-region
        :desc "Evaluate & replace region"   "E"   #'+eval:replace-region
        :desc "Format buffer/region"        "f"   #'+format/region-or-buffer
        :desc "Jump to documentation"       "k"   #'+lookup/documentation
        :n                                  "c"   nil
        :n                                  "r"   nil
        :n                                  "w"   nil
        :n                                  "W"   nil
        :n                                  "x"   nil
        )

      ;;; <leader> f --- file
      (:prefix-map ("f" . "file")
        :desc "Find file"                   "."   #'find-file
        :desc "Find file from here"         "/"
        (if (featurep! :completion ivy)
            #'counsel-file-jump
          (λ! (doom-project-find-file default-directory)))
        :n                                  "c"   nil
        :n                                  "d"   nil
        :n                                  "e"   nil
        :n                                  "E"   nil
        :n                                  "f"   nil
        :n                                  "p"   nil
        :n                                  "P"   nil
        :n                                  "r"   nil
        :n                                  "R"   nil
        :desc "Move/rename file"            "m"   #'doom/move-this-file
        :desc "Save file"                   "s"   #'save-buffer
        :desc "Sudo find file"              "S"   #'doom/sudo-find-file
        :desc "Delete this file"            "X"   #'doom/delete-this-file
        :desc "Yank filename"               "y"   #'+default/yank-buffer-filename)

      ;;; <leader> g --- git
      (:prefix-map ("g" . "git")
        :desc "Git revert file"             "R"   #'vc-revert
        (:when (featurep! :ui vc-gutter)
          :desc "Git revert hunk"           "r"   #'git-gutter:revert-hunk
          :desc "Git stage hunk"            "s"   #'git-gutter:stage-hunk
          :desc "Git time machine"          "t"   #'git-timemachine-toggle
          :desc "Jump to next hunk"         "]"   #'git-gutter:next-hunk
          :desc "Jump to previous hunk"     "["   #'git-gutter:previous-hunk)
        (:when (featurep! :tools magit)
          :desc "Magit dispatch"            "/"   #'magit-dispatch
          :desc "Forge dispatch"            "'"   #'forge-dispatch
          :desc "Magit status"              "g"   #'magit-status
          :desc "Magit file delete"         "x"   #'magit-file-delete
          :desc "Magit blame"               "B"   #'magit-blame-addition
          :desc "Magit clone"               "C"   #'+magit/clone
          :desc "Magit fetch"               "F"   #'magit-fetch
          :desc "Magit buffer log"          "L"   #'magit-log
          :desc "Git stage file"            "S"   #'magit-stage-file
          :desc "Git unstage file"          "U"   #'magit-unstage-file
          (:prefix ("f" . "find")
            :desc "Find file"                 "f"   #'magit-find-file
            :desc "Find gitconfig file"       "g"   #'magit-find-git-config-file
            :desc "Find commit"               "c"   #'magit-show-commit
            :desc "Find issue"                "i"   #'forge-visit-issue
            :desc "Find pull request"         "p"   #'forge-visit-pullreq)
          (:prefix ("o" . "open in browser")
            :desc "Browse region or line"     "."   #'+vc/git-browse-region-or-line
            :desc "Browse remote"             "r"   #'forge-browse-remote
            :desc "Browse commit"             "c"   #'forge-browse-commit
            :desc "Browse an issue"           "i"   #'forge-browse-issue
            :desc "Browse a pull request"     "p"   #'forge-browse-pullreq
            :desc "Browse issues"             "I"   #'forge-browse-issues
            :desc "Browse pull requests"      "P"   #'forge-browse-pullreqs)
          (:prefix ("l" . "list")
            (:when (featurep! :tools gist)
              :desc "List gists"              "g"   #'+gist:list)
            :desc "List repositories"         "r"   #'magit-list-repositories
            :desc "List submodules"           "s"   #'magit-list-submodules
            :desc "List issues"               "i"   #'forge-list-issues
            :desc "List pull requests"        "p"   #'forge-list-pullreqs
            :desc "List notifications"        "n"   #'forge-list-notifications)
          (:prefix ("c" . "create")
            :desc "Initialize repo"           "r"   #'magit-init
            :desc "Clone repo"                "R"   #'+magit/clone
            :desc "Commit"                    "c"   #'magit-commit-create
            :desc "Issue"                     "i"   #'forge-create-issue
            :desc "Pull request"              "p"   #'forge-create-pullreq)))

      ;;; <leader> i --- insert
      (:prefix-map ("i" . "insert")
        :desc "Insert from clipboard"         "y"   #'+default/yank-pop
        :desc "Insert from evil register"     "r"   #'evil-ex-registers
        :desc "Insert snippet"                "s"   #'yas-insert-snippet)

      ;;; <leader> n --- notes
      (:prefix-map ("n" . "notes")
        :desc "Browse notes"                  "." #'+default/browse-notes
        :desc "Search notes"                  "/" #'+default/org-notes-search
        :desc "Search notes for symbol"       "*" #'+default/search-notes-for-symbol-at-point
        :desc "Org capture"                   "c" #'org-capture
        :desc "Open deft"                     "d" nil
        :desc "Search org agenda headlines"   "h" nil
        :desc "Find file in notes"            "n" #'+default/find-in-notes
        :desc "Browse notes"                  "N" #'+default/browse-notes
        :desc "Org store link"                "l" nil
        )

      ;;; <leader> o --- open
      (:prefix-map ("o" . "open")
        :desc "Org agenda"       "A"  #'org-agenda
        (:prefix ("a" . "org agenda")
          :desc "Agenda"         "a"  #'org-agenda
          :desc "Todo list"      "t"  #'org-todo-list
          :desc "Tags search"    "m"  #'org-tags-view
          :desc "View search"    "v"  #'org-search-view)
        :n                       "b"  nil
        :n                       "d"  nil
        :n                       "r"  nil
        :n                       "R"  nil
        :desc "Dired"              "-"  #'dired-jump
        (:when (featurep! :ui treemacs)
          :desc "Project sidebar" "p" #'+treemacs/toggle
          :desc "Find file in project sidebar" "P" #'+treemacs/find-file)
        (:when (featurep! :term vterm)
          :desc "Toggle vterm popup"    "t" #'+vterm/toggle
          :desc "Open vterm here"       "T" #'+vterm/here)
        (:when (featurep! :term eshell)
          :desc "Toggle eshell popup"   "e" #'+eshell/toggle
          :desc "Open eshell here"      "E" #'+eshell/here)
        (:when (featurep! :collab floobits)
          (:prefix ("f" . "floobits")
            "c" #'floobits-clear-highlights
            "f" #'floobits-follow-user
            "j" #'floobits-join-workspace
            "l" #'floobits-leave-workspace
            "R" #'floobits-share-dir-private
            "s" #'floobits-summon
            "t" #'floobits-follow-mode-toggle
            "U" #'floobits-share-dir-public))
        (:when (featurep! :tools docker)
          :desc "Docker" "D" #'docker))

      ;;; <leader> p --- project
      (:prefix-map ("p" . "project")
        :desc "Browse project"               "." #'+default/browse-project
        :desc "Browse other project"         ">" #'doom/browse-in-other-project
        :desc "Find file in project"         "/" #'projectile-find-file
        :desc "Find file in other project"   "?" #'doom/find-file-in-other-project
        :desc "Add new project"              "a" #'projectile-add-known-project
        :desc "Compile in project"           "c" #'projectile-compile-project
        :desc "Remove known project"         "d" #'projectile-remove-known-project
        :desc "Find other file"              "o" #'projectile-find-other-file
        :desc "Switch project"               "p" #'projectile-switch-project
        :desc "Pop up scratch buffer"        "x" #'doom/open-project-scratch-buffer
        :desc "Switch to scratch buffer"     "X" #'doom/switch-to-project-scratch-buffer
        :n                                   "b" nil
        :n                                   "!" nil
        :n                                   "e" nil
        :n                                   "i" nil
        :n                                   "k" nil
        :n                                   "r" nil
        :n                                   "t" nil
        )

      ;;; <leader> q --- session
      (:prefix-map ("q" . "session")
        :desc "Quit Emacs"                   "q" #'save-buffers-kill-terminal
        :desc "Quit Emacs without saving"    "Q" #'evil-quit-all-with-error-code
        :desc "Quick save current session"   "s" #'doom/quicksave-session
        :desc "Restore last session"         "l" #'doom/quickload-session
        :desc "Save session to file"         "S" #'doom/save-session
        :desc "Restore session from file"    "L" #'doom/load-session
        :desc "Restart & restore Emacs"      "r" #'doom/restart-and-restore
        :desc "Restart Emacs"                "R" #'doom/restart)

      ;;; <leader> r --- remote
      (:when (featurep! :tools upload)
        (:prefix-map ("r" . "remote")
          :n                                 "u" nil
          :n                                 "U" nil
          :n                                 "d" nil
          :n                                 "D" nil
          :n                                 "." nil
          :n                                 ">" nil
          ))

      ;;; <leader> s --- snippets
      (:when (featurep! :editor snippets)
        (:prefix-map ("s" . "snippets")
          :desc "New snippet"                "n" #'yas-new-snippet
          :desc "Insert snippet"             "i" #'yas-insert-snippet
          :desc "Jump to mode snippet"       "/" #'yas-visit-snippet-file
          :desc "Jump to snippet"            "s" #'+snippets/find-file
          :n                                 "S" nil
          :n                                 "r" nil
          :desc "Create temporary snippet"   "c" #'aya-create
          :desc "Use temporary snippet"      "e" #'aya-expand))

      ;;; <leader> t --- toggle
      (:prefix-map ("t" . "toggle")
        :desc "Flyspell"                     "s" #'flyspell-mode
        :desc "Flycheck"                     "f" #'flycheck-mode
        :n                                   "l" nil
        :n                                   "F" nil
        :n                                   "i" nil
        :n                                   "h" nil
        :n                                   "b" nil
        :n                                   "g" nil
        :n                                   "p" nil))

;;; Universal evil integration

(when (featurep! :editor evil +everywhere)
  ;; Have C-u behave similarly to `doom/backward-to-bol-or-indent'.
  ;; NOTE SPC u replaces C-u as the universal argument.
  (map! :gi "C-u" #'doom/backward-kill-to-bol-and-indent
        :gi "C-w" #'backward-kill-word
        ;; Vimmish ex motion keys
        :gi "C-b" #'backward-word
        :gi "C-f" #'forward-word))

;; evil mode rebindings

(define-key evil-motion-state-map "j" 'evil-backward-char)
(define-key evil-motion-state-map "k" 'evil-next-line)
(define-key evil-motion-state-map "-" 'evil-forward-char)
(define-key evil-motion-state-map "l" 'evil-previous-line)

(define-key help-map "\C-a" nil)
(define-key help-map "\C-c" nil)
(define-key help-map "\C-d" nil)
(define-key help-map "\C-e" nil)
(define-key help-map "\C-f" nil)
(define-key help-map "\C-m" nil)
(define-key help-map "\C-n" nil)
(define-key help-map "\C-o" nil)
(define-key help-map "\C-p" nil)
(define-key help-map "\C-t" nil)
(define-key help-map "\C-w" nil)
(define-key help-map "F"    nil)
(define-key help-map "K"    nil)
(define-key help-map "S"    nil)
(define-key help-map "g"    nil)
(define-key help-map "4i"   nil)
(define-key help-map "n"    nil)
(define-key help-map "r"    nil)
(define-key help-map "t"    nil)

;;(define-key dired-mode-map "j" 'evil-backward-char)
;;(define-key dired-mode-map "l" 'dired-previous-line)
;;(define-key dired-mode-map "k" 'dired-next-line)
;;(define-key dired-mode-map "-" 'evil-forward-char)

(map!
 ;; delete window
 "s-q" #'delete-window

 ;; kill current buffer
 "s-c" #'kill-current-buffer

 ;; general navigation
 ;;"C-j" #'backward-char
 ;;"C-k" #'forward-line
 ;;"C-l" #'previous-line
 ;;"C--" #'forward-char
 ;;"C-p" #'recenter-top-bottom
 ;;"M-j" #'backward-word
 ;;"M-k" #'forward-paragraph
 ;;"M-l" #'backward-paragraph
 ;;"M--" #'forward-word

 ;; paragraph navigation
 ;;"M-n" #'forward-paragraph
 ;;"M-p" #'backward-paragraph

 ;; replace string
 "C-r" #'replace-string
 "C-M-r" #'replace-regexp

 ;; delete window
 "s-M-k" #'delete-window

 ;; buffer operations
 "s-C-k" #'kill-buffer

 ;; bookmarks
 "C-c b" #'bookmark-jump

 ;; display line numbers
 "C-c n" #'display-line-numbers-mode

 ;; multiple cursors
 "C-<" #'mc/mark-next-like-this
 "C->" #'mc/mark-previous-like-this

 ;; other window
 "<C-tab>" (lambda () (interactive (other-window 1)))

 ;; auto yasnippet
 "C-c a c" #'aya-create
 "C-c a e" #'aya-expand

 ;; disabling some things
 "C-z" nil
 "M-z" nil
 "C-t" nil
 )

;; custom function bindings
(map!
 "C-<backspace>"   #'cresiopan-backward-kill-char-or-word
 "C-M-<backspace>" #'cresiopan-kill-whole-line
 "s-t"             #'cresiopan-open-terminal-in-new-window
 "<f12>"           #'cresiopan-fix-text
 )

;; duplicate line with C-c C-d
(global-set-key "\C-c\C-d" "\C-a\C- \C-e\M-w\C-o\C-y  ")


(provide 'bindings)
;;; bindings.el ends here
