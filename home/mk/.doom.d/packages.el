;; -*- no-byte-compile: t; -*-
;;; private/ben/packages.el;

;;; Code:
;;(package! auto-yasnippet)
;; (("C-c a c" . aya-create)
;;  ("C-c a e" . aya-expand)))

(package! yasnippet-classic-snippets)
(package! yasnippet-snippets)

;;(use-package htmlize :ensure t)

;; (use-package org
;;   :ensure t
;;   :init
;;   ;; change postamble, to show last update date
;;   (defun my-org-html-postamble (plist)
;;     (format "Last update: %s" (format-time-string "%Y-%m-%d %H:%M")))
;;   (setq
;;    org-startup-with-inline-images t
;;    org-image-actual-width nil
;;    org-descriptive-links nil
;;    org-export-with-title nil
;;    org-html-postamble 'my-org-html-postamble
;;    org-html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"/res/org.css\"/>"
;;    org-publish-project-alist '(("blog-notes"
;;                                 :base-directory       "~/Documents/blogs/cresiopan.github.io/org/"
;;                                 :base-extension       "org"
;;                                 :publishing-directory "~/Documents/blogs/cresiopan.github.io/"
;;                                 :recursive            t
;;                                 :section-numbers      nil
;;                                 :with-author          nil
;;                                 :with-creator         nil
;;                                 :with-toc             nil
;;                                 :with-fixed-width     t
;;                                 :headline-levels      6
;;                                 :with-title           nil
;;                                 :publishing-function  org-html-publish-to-html)))

;;   :config
;;   (org-babel-do-load-languages
;;    'org-babel-load-languages '(
;;                                (gnuplot . t)(C . t)(org . t)
;;                                (ditaa . t)(lisp . t)(python . t)
;;                                (ruby . t)(dot . t)(java . t)
;;                                (js . t)(shell . t)(latex . t)
;;                                (sql . t)(sqlite . t)(emacs-lisp . t)))
;;   (setf
;;    org-html-mathjax-options
;;    '((path "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML")
;;      (linebreaks "true")
;;      (tagside "right")
;;      (align "center")
;;      (scale "100")
;;      (font "TeX")
;;      (indent "0"))))

;; (setf org-html-mathjax-template "<script src=\'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML\' async></script>")

;;(require 'ox-html)
;;(setq org-html-infojs-options '((path . "/res/org-info.js")
;;                               (view . "showall")
;;                               (toc . :with-toc)
;;                               (ftoc . "0")
;;                               (tdepth . "2")
;;                               (sdepth . "max")
;;                               (mouse . "underline")
;;                               (buttons . "0")
;;                               (ltoc . "0")
;;                               (up . "0")
;;                               (home . "0")))
;;(setq org-html-use-infojs t)

(package! drag-stuff)
;;         (drag-stuff-global-mode 1)
;;         (drag-stuff-define-keys)
