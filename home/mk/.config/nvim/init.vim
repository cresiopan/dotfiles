set nocompatible
set vb t_vb=
set ts=4
set shiftwidth=4
set ai sw=4
set expandtab
set list lcs=tab:\ \ 
set hlsearch
set number
set wrap
set ruler
set noshowmode
filetype on
filetype plugin on
filetype indent on
syntax on
set sidescroll=6
set mouse=a
set hidden
set confirm
set autowriteall
set wildmenu wildmode=full
set splitright
set splitbelow
set guioptions-=r
set guifont=Menlo\ Regular:h16

let mapleader=" "
nnoremap <silent> <leader>ec :e $MYVIMRC<CR>
nnoremap <silent> <leader>sc :source $MYVIMRC<CR>
nnoremap <silent> <leader>bb <C-^>
nnoremap <silent> <leader>bn :bn<CR>
nnoremap <C-l> :bn<CR>
nnoremap <silent> <leader>bp :bp<CR>
nnoremap <C-h> :bp<CR>
nnoremap <silent> <leader>bd :bd<CR>
nnoremap <silent> <leader>bk :bd!<CR>
nnoremap <silent> <leader>bl :ls<CR>
nnoremap <silent> <leader>bg :ls<CR>:buffer<Space>
nnoremap <silent> <leader>bh :new<CR>
nnoremap <silent> <leader>bv :vnew<CR>
nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
tnoremap <Esc> <C-\><C-n>
nnoremap <leader>m :call ToggleLightDark()<CR>

set fillchars+=vert:\ 

autocmd BufEnter * setlocal cursorline
autocmd WinEnter * setlocal cursorline
autocmd BufLeave * setlocal nocursorline
autocmd WinLeave * setlocal nocursorline

nnoremap j <left>
nnoremap k <down>
nnoremap l <up>
nnoremap ñ <right>
nnoremap ; <right>
nnoremap - <right>
vnoremap j <left>
vnoremap k <down>
vnoremap l <up>
vnoremap ñ <right>
vnoremap ; <right>
vnoremap - <right>

nnoremap <leader>w <esc>:w<cr>
nnoremap <leader>W <esc>:w<cr>
nnoremap q <esc>:q<cr>
nnoremap Q <esc>:q<cr>
set clipboard+=unnamedplus
vnoremap <C-A-c> "+y
vnoremap <leader>c "+y

set ignorecase
set smartcase
set foldmethod=marker

command Cl %s/[\t\s]\+$//g
command Clean %s/[\t\s]\+$//g
command W :w
command Q :q
command WQ :wq
command Wq :wq

let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 0
autocmd VimEnter * wincmd p

set encoding=utf-8 
set fileencodings=utf-8
